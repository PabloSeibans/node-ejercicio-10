const axios = require('axios');
const { guardarInfo, guardarInfo2, guardarInfo3 } = require("../models/guardarInfo");
const { response } = require("express");

const url = 'https://pokeapi.co/api/v2/pokemon/';
const url2 = 'https://covidtracking.com/data/api';
const url3 = 'https://jsonplaceholder.typicode.com/todos';


const lista1 = []
const lista2 = []
const lista3 = []

const pokemonAPiGet = async (req, res = response) => {

  try{
    const jsonResponse = await axios.get(url);
    const response = jsonResponse.data.results;
    // console.log(response);

    Object.keys(response).forEach(key => {
      const poke= response[key]
      lista1.push(poke)
});
  guardarInfo(lista1)
    
    res.json({
      response
    });
  } catch (error) {
    console.log(error);
    
    res.json({
      error: 'Hubo un error al consumir el servicio'
    });
  }

};


const covidTrackingGet = async (req, res = response) => {

  try{
    const jsonResponse = await axios.get(url2);
    const response = jsonResponse.data;
    // console.log(response);

    Object.keys(response).forEach(key => {
      const poke= response[key]
      lista2.push(poke)
});
  guardarInfo2(lista2)
    
    res.json({
      response
    });
  } catch (error) {
    console.log(error);
    
    res.json({
      error: 'Hubo un error al consumir el servicio'
    });
  }

};


const jsonPHGet = async (req, res = response) => {

  try{
    const jsonResponse = await axios.get(url3);
    const response = jsonResponse.data;
    // console.log(response);

    Object.keys(response).forEach(key => {
      const poke= response[key]
      lista3.push(poke)
});
  guardarInfo3(lista3)
    
    res.json({
      response
    });
  } catch (error) {
    console.log(error);
    
    res.json({
      error: 'Hubo un error al consumir el servicio'
    });
  }

};

module.exports = {
  pokemonAPiGet,
  covidTrackingGet,
  jsonPHGet
}