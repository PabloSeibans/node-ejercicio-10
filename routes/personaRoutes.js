const { Router } = require('express');
const {
  pokemonAPiGet,
  covidTrackingGet,
  jsonPHGet
} = require('../controllers/personaController');

const router = Router();

router.get('/1', pokemonAPiGet);

router.get('/2', covidTrackingGet);

router.get('/3', jsonPHGet);

module.exports = router;