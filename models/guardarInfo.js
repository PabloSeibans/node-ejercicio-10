
const fs = require('fs');
const archivo = './resultado-endpoints/sitio1.txt';
const archivo2 = './resultado-endpoints/sitio2.txt';
const archivo3 = './resultado-endpoints/sitio3.txt';
const DBRoute = './data/DB.json'

const guardarInfo = (data) => {

  fs.writeFileSync(archivo, JSON.stringify(data))

  let data2 = JSON.stringify(data, null, 2);
  fs.writeFileSync(DBRoute, data2)
}

const guardarInfo2 = (data) => {
  
  fs.writeFileSync(archivo2, JSON.stringify(data))
}


const guardarInfo3 = (data) => {

  fs.writeFileSync(archivo3, JSON.stringify(data))
}

const leerInfo = () => {
  if(!fs.existsSync(archivo)){
    return null;
   }
   console.log(archivo);
   const info = fs.readFileSync(archivo, {encoding: 'utf-8'})
  console.log('ifo', info, 'info');

   const data = JSON.parse(info);

  console.log(data, 'data ');
   return data

}


module.exports = {
  guardarInfo,
  leerInfo,
  guardarInfo2,
  guardarInfo3
};